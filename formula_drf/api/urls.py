from django.urls import path
from .views import *


urlpatterns = [
    path("teams/", TeamsApiList.as_view()),
    path("teams/<int:pk>/", TeamsApiList.as_view()),
    path("drivers/", DriversApiView.as_view()),
]
