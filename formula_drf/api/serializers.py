import io

from rest_framework import serializers
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from .models import *


# class TeamsModel:
#     def __init__(self, title, victories):
#         self.title = title
#         self.victories = victories
#
#         # имена в TeamsModel и TeamsSerializer ДОЛЖНЫ СОВПАДАТЬ !!!

class TeamsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teams
        fields = ("title", "victories", "champion")
        # fields = "__all__" - все поля


# class TeamsSerializer(serializers.Serializer):
#     title = serializers.CharField(max_length=255)
#     victories = serializers.IntegerField(default=0)
#     champion = serializers.BooleanField(default=False)
#
#     def create(self, validated_data):
#         return Teams.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.title = validated_data.get("title", instance.title)
#         instance.victories = validated_data.get("victories", instance.victories)
#         instance.champion = validated_data.get("champion", instance.title)
#         instance.save()
#         return instance


class DriversSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    birthdate = serializers.DateField()
    victories = serializers.IntegerField(default=0)
    champion = serializers.BooleanField(default=False)
    team_id = serializers.IntegerField()


# # для примера
# # переводим (кодируем) модель в json
# def encode():
#     model = TeamsModel("Mclaren", 183)
#     model_sr = TeamsSerializer(model)
#     print(model_sr.data, type(model_sr.data), sep="\n")
#     json = JSONRenderer().render(model_sr.data)
#     print(json)
#
#
# # переводим (декодируем) json в модель
# def decode():
#     stream = io.BytesIO(b'{"title":"Mclaren","victories":183}')
#     data = JSONParser().parse(stream)
#     serializer = TeamsSerializer(data=data)
#     # проверим корректность принятых данных (.is_valid)
#     serializer.is_valid()
#     print(serializer.validated_data)
