from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from django.forms import model_to_dict

from .serializers import *
from .models import *


class TeamsApiList(generics.ListCreateAPIView):
    queryset = Teams.objects.all()
    serializer_class = TeamsSerializer


class TeamsApiView(APIView):
    def get(self, request):
        w = Teams.objects.all()
        return Response({"teams": TeamsSerializer(w, many=True).data})

    def post(self, request):
        serializer = TeamsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # raise_exception сгенерирует ошибку, если проверка не пройдена
        serializer.save()
        return Response({"post": serializer.data})

        # new_team = Teams.objects.create(
        #     title=request.data["title"],
        #     victories=request.data["victories"],
        #     champion=request.data["champion"]
        # )
        # return Response({"post": TeamsSerializer(new_team).data})
        # # model_to_dict - преобразует модель в словарь

    def put(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        if not pk:
            return Response({"error": "Method PUT not allowed"})

        try:
            instance = Teams.objects.get(pk=pk)
        except:
            return Response({"error": "Object does not exists"})
        serializer = TeamsSerializer(data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"post": serializer.data})

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        if not pk:
            return Response({"error": "Method DELETE not allowed"})

        try:
            instance = Teams.objects.get(pk=pk)
            instance.delete()
        except:
            return Response({"error": "Object does not exists"})
        return Response({"post": "delete post " + str(pk)})


# class TeamsApiView(generics.ListAPIView):
#     queryset = Teams.objects.all()
#     serializer_class = TeamsSerializer
#
#
class DriversApiView(generics.ListAPIView):
    queryset = Teams.objects.all()
    serializer_class = DriversSerializer
