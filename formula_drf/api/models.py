from django.db import models


class Teams(models.Model):
    title = models.CharField(max_length=50)
    victories = models.IntegerField(default=0)
    champion = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Teams"
        verbose_name_plural = "Teams"


class Drivers(models.Model):
    title = models.CharField(max_length=100)
    birthdate = models.DateField()
    victories = models.IntegerField(default=0)
    champion = models.BooleanField(default=False)
    team = models.ForeignKey("Teams", on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Drivers"
        verbose_name_plural = "Drivers"
