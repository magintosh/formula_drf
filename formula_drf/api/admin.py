from django.contrib import admin
from .models import Teams, Drivers


admin.site.register(Teams)
admin.site.register(Drivers)
